<?php

class CustomerCommentModel extends ObjectModel
{
    public $id_comment;
    public $id_customer;
    public $comment;
    public $rate;
    public $date_add;

    public static $definition = array(
        'table' => 'customer_comments',
        'primary' => 'id_comment',
        'fields' => array(
            'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'comment' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'rate' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate')
        )
    );
}