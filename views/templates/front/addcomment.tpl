{extends file='page.tpl'}

{block name='page_content'}
    <form name="form">
        <div class="form-group">
            <label for="exampleInputPassword1">Votre message</label>
            <textarea class="form-control" rows="5" name="comment"></textarea>
        </div>
        <div class="form-group">
            <div id="stars">
                <span id="star1" class="star" data-rating="1" style="font-size:30px;color:#EAE9E6;transition:0.3s;">&bigstar;</span>
                <span id="star2" class="star" data-rating="2" style="font-size:30px;color:#EAE9E6;transition:0.3s;">&bigstar;</span>
                <span id="star3" class="star" data-rating="3" style="font-size:30px;color:#EAE9E6;transition:0.3s;">&bigstar;</span>
                <span id="star4" class="star" data-rating="4" style="font-size:30px;color:#EAE9E6;transition:0.3s;">&bigstar;</span>
                <span id="star5" class="star" data-rating="5" style="font-size:30px;color:#EAE9E6;transition:0.3s;">&bigstar;</span>
            </div>
            <textarea id="number_rating" name="rating" style="visibility: hidden; height:0px;"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Envoyer</button>
    </form>
{/block}