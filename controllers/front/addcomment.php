<?php

require_once(_PS_MODULE_DIR_ .'customercomments/models/CustomerComment.php');

class CustomerCommentsAddCommentModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $customerComment = new CustomerCommentModel();
        $customerComment->id_customer = $this->context->customer->id;

        $this->setTemplate('module:customercomments/views/templates/front/addcomment.tpl');

        $form = Tools::isSubmit('comment') && Tools::isSubmit('rating');

        if($form) {
            $customerComment->comment = Tools::getValue('comment');
            $customerComment->rate = Tools::getValue('rating');
            $customerComment->add();
        }

        parent::initContent();
    }
}