<div class="glide">
    <div class="glide__track" data-glide-el="track">
        <ul class="glide__slides">
            {foreach from=$comments item=comment}
                <li class="glide__slide card" style="box-shadow: none;">
                    <div class="card-body" style="padding:20px;">
                        <h5 class="card-title">{$comment.firstname} {$comment.lastname}</h5>
                        <p class="card-text">{$comment.comment}</p>

                        <div class="card-rating">
                            {if $comment.rate == 5}
                                <div id="stars-5">
                                    <span id="star1" class="star text-primary" data-rating="1">&bigstar;</span>
                                    <span id="star2" class="star text-primary" data-rating="2">&bigstar;</span>
                                    <span id="star3" class="star text-primary" data-rating="3">&bigstar;</span>
                                    <span id="star4" class="star text-primary" data-rating="4">&bigstar;</span>
                                    <span id="star5" class="star text-primary" data-rating="5">&bigstar;</span>
                                </div>
                            {/if}

                            {if $comment.rate == 4}
                                <div id="stars-4">
                                    <span id="star1" class="star text-primary" data-rating="1">&bigstar;</span>
                                    <span id="star2" class="star text-primary" data-rating="2">&bigstar;</span>
                                    <span id="star3" class="star text-primary" data-rating="3">&bigstar;</span>
                                    <span id="star4" class="star text-primary" data-rating="4">&bigstar;</span>
                                    <span id="star5" class="star text-muted" data-rating="5">&bigstar;</span>
                                </div>
                            {/if}

                            {if $comment.rate == 3}
                                <div id="stars-3">
                                    <span id="star1" class="star text-primary" data-rating="1">&bigstar;</span>
                                    <span id="star2" class="star text-primary" data-rating="2">&bigstar;</span>
                                    <span id="star3" class="star text-primary" data-rating="3">&bigstar;</span>
                                    <span id="star4" class="star text-muted" data-rating="4">&bigstar;</span>
                                    <span id="star5" class="star text-muted" data-rating="5">&bigstar;</span>
                                </div>
                            {/if}

                            {if $comment.rate == 2}
                                <div id="stars-2">
                                    <span id="star1" class="star text-primary" data-rating="1">&bigstar;</span>
                                    <span id="star2" class="star text-primary" data-rating="2">&bigstar;</span>
                                    <span id="star3" class="star text-muted" data-rating="3">&bigstar;</span>
                                    <span id="star4" class="star text-muted" data-rating="4">&bigstar;</span>
                                    <span id="star5" class="star text-muted" data-rating="5">&bigstar;</span>
                                </div>
                            {/if}

                            {if $comment.rate == 1}
                                <div id="stars-1">
                                    <span id="star1" class="star text-primary" data-rating="1">&bigstar;</span>
                                    <span id="star2" class="star text-muted" data-rating="2">&bigstar;</span>
                                    <span id="star3" class="star text-muted" data-rating="3">&bigstar;</span>
                                    <span id="star4" class="star text-muted" data-rating="4">&bigstar;</span>
                                    <span id="star5" class="star text-muted" data-rating="5">&bigstar;</span>
                                </div>
                            {/if}
                        </div>
                    </div>
                </li>
            </li>
            {/foreach}
        </ul>
    </div>
</div>
