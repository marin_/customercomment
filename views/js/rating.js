$(".star").click(function() {
    $(this).addClass('text-primary');
    $(this).prevAll().addClass('text-primary');
    $(this).nextAll().removeClass('text-primary');
    $("#number_rating").css("color","black");
    $("#number_rating").text($(this).attr("data-rating"));
});

$(".star").hover(function() {
    $(this).addClass('text-primary');
    $(this).css("cursor","pointer");
    $(this).prevAll().addClass('text-primary');
    $(this).nextAll().removeClass('text-primary');

    }, function() {
        $(".star[data-rating=" + $("#number_rating").html() + "]").click();
    }
);