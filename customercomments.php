<?php

// require './models/CustomerComment.php';

class customercomments extends Module
{
    public function __construct()
    {
        $this->name = 'customercomments';
        $this->author = 'IMIE';
        $this->displayName = $this->l('Customer Comments');
        $this->description = $this->l('Allow customers to write comments after they bought a product.');
        $this->version = '1.0.0';

        parent::__construct();
    }

    public function install()
    {
        return parent::install()
            && $this->installDB()
            && $this->registerHook('displayHome')
            && $this->registerHook('displayHeader')
            && $this->registerHook('displayCustomerAccount');
    }

    public function installDB()
    {
        return Db::getInstance()->execute(
            'CREATE TABLE `' . _DB_PREFIX_ . 'customer_comments` (
                id_comment int AUTO_INCREMENT PRIMARY KEY ,
                id_customer int,
                comment varchar(1000),
                rate int,
                date_add DATE
              )                                   
        ');
    }

    public function uninstall()
    {
        return parent::uninstall()
            && $this->uninstallDB();
    }

    public function uninstallDB()
    {
        return Db::getInstance()->execute(
            'DROP TABLE `' . _DB_PREFIX_ . 'customer_comments`'
        );
    }

    public $tabs = array(
        array(
            'name' => 'Commentaires',
            'class_name' => 'AdminCustomerComments',
            'visible' => true,
            'parent_class_name' => 'ShopParameters',
        )
    );

    public function hookDisplayCustomerAccount()
    {
        return $this->display(__FILE__, 'customer_account.tpl');
    }

    public function hookDisplayHome()
    {
        $comments = Db::getInstance()->executeS(
            'SELECT * 
                  FROM '. _DB_PREFIX_ .'customer_comments
                  LEFT JOIN '. _DB_PREFIX_ .'customer ON '. _DB_PREFIX_ .'customer_comments.id_customer = '. _DB_PREFIX_ .'customer.id_customer');

        $this->context->smarty->assign([
            'comments' => $comments
        ]);

        return $this->display(__FILE__, 'home.tpl');
    }

    public function hookDisplayHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/rating.js');
        $this->context->controller->addJS($this->_path.'/views/js/slider.js');

        $this->context->controller->registerJavascript(
            'remote-bootstrap',
            'https://cdn.jsdelivr.net/npm/@glidejs/glide',
            ['server' => 'remote', 'priority' => 20]
        );

        $this->context->controller->registerStylesheet(
            'remote-bootstrap',
            'https://cdn.jsdelivr.net/npm/@glidejs/glide@3.2.6/dist/css/glide.core.min.css',
            ['server' => 'remote', 'position' => 'head',  'priority' => 20]
        );
    }
}