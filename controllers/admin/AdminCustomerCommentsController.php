<?php

require_once _PS_MODULE_DIR_ . '/customercomments/models/CustomerComment.php';

class AdminCustomerCommentsController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = CustomerCommentModel::$definition['table'];
        $this->identifier = CustomerCommentModel::$definition['primary'];
        $this->className = CustomerCommentModel::class;

        parent::__construct();

        $this->_select .= ('firstname, lastname');

        $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'customer` s ON s.`id_customer` = a.`id_customer`';


        $this->fields_list = [
            'id_comment' => [
                'title' => $this->module->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ],
            'lastname' => [
                'title' => $this->module->l('Nom'),
                'align' => 'left',
            ],
            'firstname' => [
                'title' => $this->module->l('Prénom'),
                'align' => 'left',
            ],
            'comment' => [
                'title' => $this->module->l('Commentaire'),
                'align' => 'left',
            ],
            'rate' => [
                'title' => $this->module->l('Note'),
                'align' => 'left',
            ],
            'date_add' => [
                'title' => $this->module->l('Date'),
                'align' => 'left',
            ]
        ];
        $this->addRowAction('edit');
        $this->addRowAction('delete');
    }
}
