new Glide('.glide', {
    type: 'carousel',
    startAt: 0,
    focusAt: 'center',
    perView: 3,
    autoplay: 2000,
}).mount()